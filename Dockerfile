FROM node:14

COPY src ./src
COPY package.json ./
COPY .babelrc ./
RUN npm install

CMD ["npm", "start"]

EXPOSE $PORT
