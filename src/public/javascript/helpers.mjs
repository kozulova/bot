export const test = "testing of modules";

export const getText = async (id) =>{
    const url = `/game/texts/${id}`;
    const response = await fetch(url);
    const text = await response.json();
    return text;
  }

export  const createUser = (name) =>{
    const userDiv = document.createElement('div');
    const li = document.createElement('li');
    li.innerHTML += name;  
    const progress = document.createElement('progress');
    progress.setAttribute('max', '100');
    progress.setAttribute('value', 0);
    progress.setAttribute('progressUsername', name);
    userDiv.appendChild(li);
    userDiv.appendChild(progress);
    return userDiv;
  }
  
export  const setProgress = (progress, username) =>{
    const progressBar = document.querySelector(`[progressUsername=${username}]`);
    progressBar.setAttribute('value', progress);
  }

export const createRoomElement = (room) => {
    let roomElement = document.createElement('div');
    let chatUsers = room.users.map(user=>`<span>${user}</span>`);
    const roomCard = `
    <h2>${room.id}</h2>
    <h3>${room.name}</h3>
    <div>${chatUsers}</div>
    <button class="join" id="${room.id}">Join</button>
    `;
    roomElement.classList.add('room_card');
    roomElement.innerHTML += roomCard;
    return roomElement;
  }

export const renderBotmessage = (message) =>{
    const speechBubble = document.querySelector('.speech-bubble');
    speechBubble.innerHTML = '';
    speechBubble.innerHTML += message;
}