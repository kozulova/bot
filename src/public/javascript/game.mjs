import {getText, createUser, setProgress, createRoomElement, renderBotmessage} from './helpers.mjs';

const SECONDS_TIMER_BEFORE_START_GAME = 10;

const username = sessionStorage.getItem("username");
let activeRoomId = null;

if (!username) {
  window.location.replace("/login");
}

const socket = io("http://localhost:3002/", { query: { username } });

socket.emit('check', username, (data) => {
  if(data==='error'){
    userNameUsedNotify(username);
  }
});

const userNameUsedNotify = (username) => {
  window.alert(`${username} is already in use`);
  sessionStorage.clear();
  window.location.replace("/login");
}

const renderRooms = (rooms) => {
  const roomsPage = document.getElementById('rooms-page');
  roomsPage.innerText = '';
  rooms.forEach(room => {
    roomsPage.appendChild(createRoomElement(room));
  })
  joinRoomListeners();
}

const joinRoomListeners = () => {
  const joinRoomBtns = document.querySelectorAll('.join');
  joinRoomBtns.forEach(btn => btn.addEventListener('click', startJoinRoom));
}

const startJoinRoom = (e)=>{
  const roomId = e.target.id;
  activeRoomId = roomId;
  toogleGamePage();
  socket.emit('JOIN_ROOM', roomId, username, (data)=>{
    if(data='error'){
      console.log('error');
    }
  })
}

const addRoomsListeners = () => {
  const addRoomBtn = document.getElementById('add-room-btn');
  let roomname;
  addRoomBtn.addEventListener('click', ()=>{
    roomname = prompt('Plase provide room name');
    if(roomname){
      socket.emit('ADD_NEW_ROOM', roomname);
    }
  })
}

addRoomsListeners();

socket.on('UPDATE_ROOMS', (data)=>{
  renderRooms(data);
});

const joinRoomDone = (room) => {
 renderGamePage(room);
}

const renderGamePage = (room) => {

  renderBotmessage(`Hello ${username}, I am your bot`);

  const backBtn = document.getElementById('back');
  backBtn.addEventListener('click', ()=>{
    socket.emit('LEAVE_ROOM', activeRoomId);
    toogleGamePage();
  });
  
  const users_list = document.querySelector('.users_list');
  room.users.forEach(name=>{
    const userLi = createUser(name);
    users_list.appendChild(userLi);
  })
  const ready_btn = document.getElementById('ready_btn');
  ready_btn.addEventListener('click', startGame);
}

const toogleGamePage = () => {
  const gamePage = document.getElementById('game-page');
  const roomPage = document.getElementById('rooms-page');
  const addRoomBtn = document.getElementById('add-room-btn');
  const backBtn = document.getElementById('back');

  gamePage.classList.toggle('display-none');
  roomPage.classList.toggle('display-none');
  addRoomBtn.classList.toggle('display-none');
  backBtn.classList.toggle('display-none');
}

const startGame = async () => {
  const text = await getText(2);
  const ready_btn = document.getElementById('ready_btn');
  ready_btn.style.display = 'none';
  const timerEl = document.getElementById('timer');
  timerEl.style.display = 'block';
  let secs = SECONDS_TIMER_BEFORE_START_GAME;
  const timerFunction = () => {
    timerEl.innerText = '';
    timerEl.innerHTML += secs;
    secs--;
    if(secs<0){
      clearInterval(timer);
      socket.emit('START_GAME', activeRoomId);
      startTyping(text);
    }
  };
 const timer = setInterval(timerFunction, 100)
  
}

const startTyping = ({text}) => {
  const gamePage = document.querySelector('.textblock');
  const textDiv = document.createElement('div');
  textDiv.innerHTML += text;
  gamePage.innerHTML = '';
  gamePage.appendChild(textDiv);
  const textArray = text.split('');
  const typedChars = [];

  document.addEventListener('keyup', (e)=>{
      if(textArray[0] == e.key){
      typedChars.push(textArray[0]);
      textArray.shift();
    }
    renderString(textArray, typedChars);
    let progress = typedChars.length/text.length  * 100
    setProgress(progress, username);
    socket.emit('PROGRESS', progress, textArray, typedChars);
    
    if(textArray.length === 0){
      socket.emit('WIN', activeRoomId);
    }

  });
}

const renderString = (textArray, typedChars) => {
  const gamePage = document.querySelector('.textblock');
  const textDiv = document.createElement('div');

  const matchedTextSpan = document.createElement('span');
  matchedTextSpan.style.backgroundColor = 'green';
  matchedTextSpan.innerHTML += typedChars.join('');

  const leftCharacters = document.createElement('span');
  leftCharacters.innerHTML += textArray.join('');

  gamePage.innerHTML = '';
  gamePage.appendChild(matchedTextSpan);  
  gamePage.appendChild(leftCharacters);
}

socket.on('GAMEOVER', (user)=> {
  alert(user);
  toogleGamePage();
});
socket.on('JOIN_ROOM_DONE', joinRoomDone);

socket.on('BOTSPEAKING', (message)=>{
  renderBotmessage(message);
})