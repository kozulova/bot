import * as config from "./config";
import { Room } from '../clases/Room';
import { Bot, History } from '../clases/Bot';

const users = [];
const history = [];

const room1 = new Room(1, 'room1', ['t1', 't2']);
const room2 = new Room(2, 'room2', ['t1', 't2']);
let rooms = [room1, room2];

export default io => {
  io.on('connection', socket => {
    const username = socket.handshake.query.username;
// Check login //
    socket.on('check', (name, fn) => {
      if (users.indexOf(name) < 0) {
        users.push(name);
        fn('success');
      }
      else {
        fn('error')
      }
    });

    socket.emit("UPDATE_ROOMS", rooms);

    socket.on('ADD_NEW_ROOM', (roomName) => {
      const newRoom = new Room(rooms[rooms.length - 1].id + 1, roomName, [username]);
      rooms.push(newRoom);
      io.emit('UPDATE_ROOMS', rooms);
    });

    socket.on('JOIN_ROOM', (roomId)=>{
      const room = rooms.find(room=>room.id == roomId);
      socket.join(roomId, () => {
        room.addUsers(username);
        rooms = rooms.map(r=> r.id == room.id ? room : r);
        io.to(socket.id).emit("JOIN_ROOM_DONE", room);
        socket.emit("UPDATE_ROOMS", rooms);
      });

      const bot = new Bot(room);
      socket.on('PROGRESS', (progress)=>{
        const message = bot.updateStatus(progress, username, 200);
        io.to(roomId).emit('BOTSPEAKING', message);
      })

      socket.on('WIN', (roomId)=>{
        const historyObj = new History(username, 60).writeRowToHistory();
        history.push(historyObj);
        io.to(roomId).emit('GAMEOVER', historyObj);
        const room = rooms.find(room=>room.id == roomId);
        room.users = room.users.filter(user => user !== username)
        socket.leave(roomId);
        io.emit('UPDATE_ROOMS', rooms);
      })

    })

    socket.on('LEAVE_ROOM', (roomId)=>{
      const room = rooms.find(room=>room.id == roomId);
      room.users = room.users.filter(user => user !== username)
      socket.leave(roomId);
      io.emit('UPDATE_ROOMS', rooms);
    })

    socket.on('disconnect', () => {
      console.log(`${socket.id} is disconnected`);
    })
  })

}