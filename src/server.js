import express, { text } from "express";
import http from "http";
import socketIO from "socket.io";
import socketHandler from "./socket";
import routes from "./routes";
import { STATIC_PATH, PORT } from "./config";
import {texts} from './data';

const app = express();
const httpServer = http.Server(app);
const io = socketIO(httpServer);

app.use(express.static(STATIC_PATH));

app.get('/game/texts/:id', (req, res) => {
  const id = req.params.id;
  const text = texts[id];
  return res.status(200).send({text});
})

routes(app);

app.get("*", (req, res) => {
  res.redirect("/login");
});

socketHandler(io);

httpServer.listen(PORT, () => {
  console.log(`Listen server on port ${PORT}`);
});

export { app, httpServer };