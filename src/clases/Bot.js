class Score {
    constructor(progress) {
        this.progress = progress;
    }
    isCloseWin() {
        return this.progress > 50
    }
    isWin() {
        return this.progress > 99
    }
    /* functional programming
    returnAllScore(){
        return this.users.map(user => `${user}'s score is`)
    }
    checkWinner(){
        return this.users.reduce((winner, current, index, array)=>{current.score>array[index-1]?current:array[index-1]}, '');
    }
    */
}

class gameState {
    constructor(time) {
        this.time = time;
    }
    isTimeEnds() {
        return this.time < 100;
    }
}

// Proxy
class History {
    constructor(winner, time) {
        this.winner = winner;
        this.time = time;
    }
    handler = {
        get: function (target, prop, receiver) {
            return target.winner + ' User won with';
        }
    };
    writeRowToHistory() {
        const proxy = new Proxy(this, this.handler);
        return proxy.winner;
    }
}

// Facade 
class Message {
    applyForMessageStructure(progress, time) {
        const winClose = new Score(progress).isCloseWin();
        const timer = new gameState(time).isTimeEnds();
        //console.log(progress, 'close to win');
        return winClose ? 'Close to finish' : timer ? 'Time is ending' : 'I beliave in you!'
    }
}

class Bot {
    constructor(room) {
        this.room = room
    }
    updateStatus(progress, username, time) {
        return new Message().applyForMessageStructure(progress, time);
        //return `There are ${this.room.users.length} count of users and ${username} has ${progress}`
    }
}
export { Bot, History }