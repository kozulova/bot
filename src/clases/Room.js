class Room {
    constructor(id, name, users){
        this.id = id
        this.name = name;
        this.users = users;
    }
    addUsers = (user) => {
        this.users = [...this.users, user];
    }
    returnRoom = () => {
        return {id: this.id, name: this.name, users: this.users}
    }
}

export {Room}